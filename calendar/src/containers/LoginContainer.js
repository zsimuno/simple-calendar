import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';
import { LoginComponent } from '../components/LoginComponent';

@observer
export class LoginContainer extends Component {

    @observable
    componentState = {
        errors: [],
        failed: false,
    };

    @action.bound
    onSuccess(response) {
        sessionStorage.setItem('accessToken', response.Zi ? response.Zi.access_token : response.accessToken);
        sessionStorage.setItem('email', response.profileObj.email);
        this.props.history.push('./calendar');
        }

    @action.bound
    onFailure(response) {
        console.log("Failure");
        console.log(response);
        this.componentState.errors = response;
        this.componentState.failed = true;
        }

    render() {
        return (
        <LoginComponent 
            onSuccess={this.onSuccess}
            onFailure={this.onFailure}
            failed={this.componentState.failed}
        />
            );
        }
}
