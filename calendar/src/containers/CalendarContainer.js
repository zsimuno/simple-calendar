import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, runInAction } from 'mobx';
import { HeaderComponent } from '../components/HeaderComponent';
import { CalendarComponent } from '../components/CalendarComponent';
import { IntervalChangerComponent } from '../components/IntervalChangerComponent';
import { getAllEvents, deleteEvent, addEvent } from '../services/events';
import { AddEventComponent } from '../components/AddEventComponent';

@observer
export class CalendarContainer extends Component {

    @observable
    componentState = {
        currentTimeInterval: 7,
        events: [],
        getErrors: undefined,
        addErrors: undefined,
        deleteErrors: undefined,
        response: {},
        eventName: '',
        startTime: '',
        endTime: '',
        showAddForm: false,
        deleteButtonPressed: {},
        loggedIn: true,
    };

    @action.bound
    onIntervalChange(event) {
        this.componentState.currentTimeInterval = parseInt(event.currentTarget.value, 10);
    }

    eventSort(a, b) {
        const first = a.start.date || a.start.dateTime;
        const second = b.start.date || b.start.dateTime;
        return (new Date(first)) - (new Date(second));
        }


    @action.bound
    deleteSelectedEvent(option, eventId) {

        switch (option) {
            case "showDelete":
                this.componentState.deleteButtonPressed[eventId] = true;
                break;
                
            case "cancel":
                this.componentState.deleteButtonPressed[eventId] = false;
                this.componentState.deleteErrors = undefined;
                    break;
        
            default:
                deleteEvent(eventId, this.componentState, sessionStorage.getItem("accessToken"))
                    .then(() => runInAction(() => {
                        if(this.componentState.response.error === undefined) {
                            this.componentState.events = this.componentState.events.filter(event => event.id !== eventId);
                            this.componentState.deleteErrors = undefined;
                        } else {
                            this.componentState.deleteErrors = this.componentState.response.error.errors;
                        }
                        
                    }));
                break;
        }
    }

    @action.bound
    addEvent(option, event) {
        switch (option) {

            case "add": {
                event.preventDefault();

                if(!this.componentState.eventName) {
                    this.componentState.addErrors =[{message: "Please input event name!"}];
                    return;
                }

                let start = Date.parse(this.componentState.startTime);
                let end = Date.parse(this.componentState.endTime);

                if(isNaN(start) || isNaN(end)) {
                    this.componentState.addErrors = [{message: "Invalid date input!"}];
                    return;
                }

                if(start > end) {
                    this.componentState.addErrors = [{message: "Start time must be before the end!"}];
                    return;
                }

                let eventData = {
                    'summary': this.componentState.eventName,
                    'start': {
                        'dateTime': (new Date(start)).toISOString()
                    },
                    'end': {
                        'dateTime': (new Date(end)).toISOString()
                    }
                };

                addEvent(eventData, this.componentState, sessionStorage.getItem("accessToken"))
                    .then(() => runInAction(() => {
                        if(this.componentState.response.error === undefined) {
                            this.componentState.events.push(this.componentState.response);
                            this.componentState.events.replace(this.componentState.events.slice().sort(this.eventSort));
                            this.componentState.addErrors = undefined;
                            this.resetAddForm();
                        } else {
                            this.componentState.addErrors = this.componentState.response.error.errors;
                        }
        
                    }));
                break;
            }

            case "cancel":
                this.resetAddForm();
                this.componentState.addErrors = undefined;
                break;

            case "showForm":
                this.componentState.showAddForm = true;
                break;
                
            default:
                break;
        }
    }

    @action.bound
    resetAddForm() {
        this.componentState.showAddForm = false;
        this.componentState.eventName = '';
        this.componentState.startTime = '';
        this.componentState.endTime = '';
    }

    @action.bound
    onChangeInput(field) {
        return action((event) => {
            this.componentState[field] = event.target.value;
        });
    }


    @action.bound
    componentDidMount() {
        getAllEvents(this.componentState, sessionStorage.getItem("accessToken"))
            .then(() => runInAction(() => {
                if(this.componentState.response.error === undefined) {
                    this.componentState.events.replace(this.componentState.response["items"].slice().sort(this.eventSort));
                    this.componentState.getErrors = undefined;
                } else {
                    this.componentState.getErrors = this.componentState.response.error.errors;
                }
            }));
    }

    render() {
        return (
            <div >
                <HeaderComponent state={this.componentState} /> 
                <IntervalChangerComponent 
                    onIntervalChange={this.onIntervalChange} 
                    currentTimeInterval={this.componentState.currentTimeInterval}
                />
                <CalendarComponent 
                    currentTimeInterval={this.componentState.currentTimeInterval}
                    events={this.componentState.events}
                    deleteSelectedEvent={this.deleteSelectedEvent}
                    deleteButtonPressed={this.componentState.deleteButtonPressed}
                    deleteErrors={this.componentState.deleteErrors}
                    getErrors={this.componentState.getErrors}
                />
                <AddEventComponent 
                    onChangeInput={this.onChangeInput}
                    onSubmit={(event) => this.addEvent('add', event)} 
                    onCancel={() => this.addEvent('cancel')} 
                    onShowForm={() => this.addEvent('showForm')}
                    eventName={this.componentState.eventName}
                    startTime={this.componentState.startTime}
                    endTime={this.componentState.endTime}
                    showAddForm={this.componentState.showAddForm}
                    addErrors={this.componentState.addErrors}
                    />
            </div>

        );
    }
}