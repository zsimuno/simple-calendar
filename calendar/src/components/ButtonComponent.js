import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { css } from 'emotion';
import { Link } from 'react-router-dom';
import { primaryColorDark, primaryColorLight } from '../style';

const buttonStyle = css`
    padding: 10px 22px;
    margin: 5px;
    border-radius: 8px;
    background-color: ${primaryColorDark};
    color: white;
    &:hover {
        background-color: ${primaryColorLight};
        color: black;
        cursor: pointer;
    }
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
`;

@observer
export class ButtonComponent extends Component {
    render() {
        const {type, onClick, text, disabled, linkTo} = this.props;

        const button = <button
                        className={buttonStyle}
                        type={type}
                        onClick={onClick}
                        disabled={disabled}
                        >
                            {text}
                        </button>;
        return (
            !linkTo ?
                button
                :
                <Link to={linkTo}>
                    {button}
                </Link>
        );
    }
}
