import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { css } from 'emotion';

const radioButton = css`
    display: none;
`;

const color = `#ec407a`;

const label = (checked) => css`
    color: white;
    border-radius: 8px;
    background-color: #b4004e; 
    ${checked && (`background-color: ${color}; color: black;` )}
    &:hover {
        background-color: ${color};
        color: black;
        cursor: pointer;
    }
    padding: 20px;
`;

const container = css`
    display: flex;
    padding: 20px;
    justify-content: space-evenly;
`;

const displayText = css`
    font-size: 35px;
`;

@observer
export class IntervalChangerComponent extends Component {
    render() {
        const { onIntervalChange, currentTimeInterval } = this.props;
        return (
            <div className={container}>
                <div className={displayText}>Display:</div>
                <input 
                    className={radioButton}
                    type="radio" 
                    name="interval" 
                    value="1"  
                    id="radio1"
                    onChange={onIntervalChange} 
                    checked={currentTimeInterval === 1} 
                /> 
                <label className={label(currentTimeInterval === 1)} htmlFor="radio1">Day</label>
                <input 
                    className={radioButton}
                    type="radio" 
                    name="interval"
                    value="7"   
                    id="radio17"
                    onChange={onIntervalChange} 
                    checked={currentTimeInterval === 7} 
                />
                <label className={label(currentTimeInterval === 7)} htmlFor="radio17">Week</label>
                <input 
                    className={radioButton}
                    type="radio"
                    name="interval" 
                    value="30"   
                    id="radio30"
                    onChange={onIntervalChange}
                    checked={currentTimeInterval === 30} 
                /> 
                <label className={label(currentTimeInterval === 30)} htmlFor="radio30">Month</label>
                 <br/>
            </div>

        );
    }
}

