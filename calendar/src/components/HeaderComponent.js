import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { GoogleLogout } from 'react-google-login';
import { css } from 'emotion';
import { primaryColor } from '../style';
import { ButtonComponent } from './ButtonComponent';
import { logout, failLogout } from '../services/user';
import { Redirect } from 'react-router-dom';

const headerContainer = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 50px;
    color: white;
    padding: 20px;
    background-color: ${primaryColor};
`;

const mailLogout = css`
    display: flex;
    flex-direction: column;
    justify-content: right;
    align-content: space-between;
`;

@observer
export class HeaderComponent extends Component {

    render() {
        const { hideLogout, state } = this.props;

        if(state !== undefined && state.loggedIn === false) {
            return <Redirect to='/login' />;
        }

        return (
                <header className={headerContainer} >

                    <h1>Simple Calendar</h1>
                    {
                        !hideLogout &&
                        <div className={mailLogout}>
                            <div>{sessionStorage.getItem('email')}</div>
                            <br />
                            <GoogleLogout
                                clientId="547747977463-nrttiqcmv2n0sis3ot296gho657vdbl0.apps.googleusercontent.com"
                                buttonText="Logout"
                                onLogoutSuccess={() => {
                                    logout(state);
                                }}
                                onFailure={failLogout}
                                render={renderProps => (
                                    <ButtonComponent 
                                    onClick={renderProps.onClick} 
                                    disabled={renderProps.disabled}
                                    text="Logout"
                                    />
                                  )}
                                redirectUri="/login"
                            />
                        </div>
                    }
                </header>

        );
    }
}
