import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { EventComponent } from './EventComponent';
import { css } from 'emotion';
import { primaryColorDark, primaryColorLight } from '../style';

const container = css`
    padding: 20px;
    ul {
        list-style-type: none;
      }
`;

const listsContainer = css`
    display: flex;
    flex-wrap: wrap;
`;

const oneGroup = css`
    border: 1px solid ${primaryColorLight};
    margin: 5px;
`;

const oneEvent = css`
    border-bottom: 2px solid #fce4ec;
    margin-bottom: 10px;
`;

const eventList = css`
    
    padding: 15px;
`;

const groupTitle = css`
    background-color: ${primaryColorDark};
    color: white;
    padding: 10px;
`;

@observer
export class CalendarComponent extends Component {

    datesAreOnSameDay = (first, second) =>
        first.getFullYear() === second.getFullYear() &&
        first.getMonth() === second.getMonth() &&
        first.getDate() === second.getDate();

    render() {
        const {  deleteButtonPressed, 
            deleteSelectedEvent, 
            events, 
            currentTimeInterval,
            deleteErrors,
            getErrors, 
        } = this.props;
        

        if(getErrors) {
            return (
                <div>
                    Unable to get events!
                    {
                        getErrors.map((e) => 
                            <div key={e.message}> {e.message}</div>
                            )
                    }
                </div>
            );
        }

        // Create groups of event. Group them by day or week.
        let eventGroups = [];
        let counter = new Date();
        counter.setHours(0);
        counter.setMinutes(0);
        counter.setSeconds(1);

        let loopLimit = 1;
        let dateCnt = 1;

        if(currentTimeInterval === 7) {
            loopLimit = 7;
        } else if (currentTimeInterval === 30) {
            loopLimit = 5;
            dateCnt = 7;
        }

        for(let i = 0; i < loopLimit; ++i, counter.setDate(counter.getDate() + dateCnt)) {
            eventGroups.push({
                day: new Date(counter),
                events: [],
            });
        }

        // Put each event in its appropriate group.
        const now = new Date();

        for (const e of events) {
            const startDate = new Date(e.start.date || e.start.dateTime);
            const dayDiff = Math.floor((startDate.getTime() - now.getTime())/ (1000 * 3600 * 24)); 
            if(dayDiff < 0 && !this.datesAreOnSameDay(now, startDate)) {
                continue;
            }

            if(dayDiff >= currentTimeInterval) {
                break;
            }
            
            if(currentTimeInterval === 30) {
                for (let i = 0; i < eventGroups.length; i++) {
                    const group = eventGroups[i];
                    const weekDif = Math.floor((startDate.getTime() - group.day.getTime())/ (1000 * 3600 * 24));

                    if(weekDif >= 0 && weekDif < 7) {
                        group.events.push(e);
                    }
                }
            } else {
                
                for(const group of eventGroups){
                    if(this.datesAreOnSameDay(group.day, startDate)) {
                        group.events.push(e);
                        break;
                    }
                }
            }
        }

        return (
            <div className={container}>
                <ul className={listsContainer}>
                    {
                        eventGroups.map((group) => 
                        <li key={group.day.getTime()} className={oneGroup}>
                            <div className={groupTitle}>
                                {(currentTimeInterval === 30 ?   "Week: " : "Day: ") + 
                                group.day.toLocaleDateString()}
                            </div>
                            
                            <ul className={eventList}> {
                                group.events.length === 0 ? 
                                "No events"
                                :
                                group.events.map((event) => {
                                    const eventId = event.id;
                                    return (
                                        <li key={eventId} className={oneEvent} >
                                            <EventComponent 
                                                event={event}
                                                deleteButtonPressed={deleteButtonPressed[eventId]} 
                                                deleteSelectedEvent={deleteSelectedEvent}
                                                deleteErrors={deleteErrors}
                                            />
                                        </li>
                                    ); 
                            }) } 
                            </ul>
                        </li>)
                    }
                </ul>
            </div>

        );
    }
}
