import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { ButtonComponent } from './ButtonComponent';

@observer 
export class EventComponent extends Component {
    render() {
        const { event, deleteSelectedEvent, deleteButtonPressed, deleteErrors } = this.props;

        const startDate = new Date(event.start.date || event.start.dateTime);
        const endDate = new Date(event.end.date || event.end.dateTime);

        return (
            <table> 
            <tbody>
                <tr>
                    <th>Name:</th>
                    <td>{event.summary}</td>
                </tr>
                <tr>
                    <th>Start:</th>
                    <td>{startDate.toLocaleString()}</td>
                </tr>
                <tr>
                    <th>End:</th>
                    <td>{endDate.toLocaleString()}</td>
                </tr>
                <tr>
                    <td colSpan="2">
                    {
                        deleteButtonPressed ?
                        <div>
                            Are you sure?
                            <br />
                            <ButtonComponent
                                text="Yes"
                                onClick={() => deleteSelectedEvent("delete", event.id)}
                            />
                            <ButtonComponent
                                text="Cancel"
                                onClick={() => deleteSelectedEvent("cancel", event.id)}
                            />
                        </div>
                        :
                        <div>
                        <ButtonComponent
                            text="Delete"
                            onClick={() => deleteSelectedEvent("showDelete", event.id)}
                        />
                        {deleteErrors && 
                            <ul>
                                Unable to delete event!
                                {deleteErrors.map((e) => 
                                <li  key={e.message}>{e.message}</li>
                                    )}
                            </ul>}
                        </div>
                        
                    }
                    </td>
                </tr>
            </tbody>
            </table>
        );
    }
}
