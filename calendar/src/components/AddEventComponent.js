import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { ButtonComponent } from './ButtonComponent';
import { css } from 'emotion';
import { customInput, inputLabel } from '../style';

const container = css`
    padding: 30px;
`;

@observer
export class AddEventComponent extends Component {

    render() {
        const { 
            onSubmit, 
            onCancel, 
            onShowForm,
            eventName, 
            startTime,
            endTime,
            onChangeInput,
            showAddForm,
            addErrors,
        } = this.props;

        return (
            <div className={container}>
            {   !showAddForm ? 
                <ButtonComponent 
                    onClick={onShowForm} 
                    text={"Add new event!"}
                    />
                :
                <div >
                    <h1>Add event</h1>
                    <form onSubmit={onSubmit}>
                        <label
                            htmlFor="eventName"
                            className={inputLabel}
                        >
                                    Event name:
                        </label> 
                        <br />
                        <input
                            type="text"
                            id="eventName"
                            className={customInput}
                            value={eventName}
                            onChange={onChangeInput('eventName')}
                            autoFocus
                        />
                        <br />
                        <label
                            htmlFor="startTime"
                            className={inputLabel}
                        >
                                    Start time:
                        </label> 
                        <br />
                        <input
                            type="date"
                            id="startTime"
                            className={customInput}
                            value={startTime}
                            onChange={onChangeInput('startTime')}
                        />
                        <br />
                        <label
                            htmlFor="endTime"
                            className={inputLabel}
                        >
                                    End time:
                        </label> 
                        <br />
                        <input
                            type="date"
                            id="endTime"
                            className={customInput}
                            value={endTime}
                            onChange={onChangeInput('endTime')}
                        />
                        <br />
                        <ButtonComponent 
                            text="Submit"
                            type='submit'
                        />

                        <ButtonComponent 
                            text="Cancel"
                            onClick={onCancel}
                        />
                        {addErrors && 
                        <div>
                            Unable to add event!
                            {addErrors.map((e) => 
                            <div  key={e.message}>{e.message}</div>
                                )}
                        </div>}
                    </form>
                </div>
            }
            </div>
        );
    }
}
