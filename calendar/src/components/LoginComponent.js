import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { HeaderComponent } from '../components/HeaderComponent';
import { GoogleLogin } from 'react-google-login';
import { Redirect } from 'react-router-dom';
import { ButtonComponent } from '../components/ButtonComponent';
import { css } from 'emotion';

const googleLogin = css`
    padding: 20px;
`;

@observer
export class LoginComponent extends Component {

    render() {
        const { onSuccess, onFailure, failed } = this.props;

        if(sessionStorage.getItem("email")) {
            return <Redirect to="/calendar"/>;
        }
        
        return (
            <div>
                <HeaderComponent hideLogout /> 
                <div className={googleLogin}>
                    <h3>Login with your Google account:</h3>
                    
                    <GoogleLogin
                        clientId="547747977463-nrttiqcmv2n0sis3ot296gho657vdbl0.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={onSuccess}
                        onFailure={onFailure}
                        render={renderProps => (
                            <ButtonComponent 
                            onClick={renderProps.onClick} 
                            disabled={renderProps.disabled}
                            text="Google login"
                            />
                        )}
                        cookiePolicy={'single_host_origin'}
                        scope="https://www.googleapis.com/auth/calendar.events"
                    />
                    {
                        failed &&
                        <div>Login failed! Please try again.</div>
                    }
                </div>
            </div>
        );
    }
}
