import { css } from 'emotion';

export const primaryColor = `#c2185b`;
export const primaryColorDark = `#8c0032`;
export const primaryColorLight = `#fa5788`;

export const customInput = css`
    border: none;
    outline: none;
    border-bottom: 1px solid ${primaryColorLight};
    font-size: 20px;
    margin: 15px;
    `;

export const inputLabel = css`
    font-size: 20px;
    
`;


