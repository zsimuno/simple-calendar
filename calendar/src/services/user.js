import { runInAction } from "mobx";

export function logout(state) {
    sessionStorage.clear();
    runInAction(() => state.loggedIn = false);
}

export function failLogout(response) {
    console.log(response);
}