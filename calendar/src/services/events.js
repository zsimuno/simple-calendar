import { post, get, deleteApi } from './api';
import { runInAction } from 'mobx';

export async function getAllEvents(state, accessToken) {
    const response = await get(`calendars/primary/events/`, accessToken);
    runInAction(() => {
        state.response = response;
    });
}

export async function addEvent(eventData, state, accessToken) {
    const response = await post(`calendars/primary/events`, eventData, accessToken);
    runInAction(() => {
        state.response = response;
    });
}


export async function deleteEvent(eventID, state, accessToken) {
    const response = await deleteApi(`calendars/primary/events/${eventID}`, accessToken);
    runInAction(() => {
        state.response = response;
    });
}