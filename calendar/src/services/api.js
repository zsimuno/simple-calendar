
export function get(model, accessToken) {
    return fetch(`https://www.googleapis.com/calendar/v3/${model}`, {
      method: 'GET',
      headers: {
        'Authorization': "Bearer " + accessToken,
      },
    })
    .then((response) => response.json())
    .catch((err) => err);
}


export function post(model, data, accessToken) {
    return fetch(`https://www.googleapis.com/calendar/v3/${model}`, {
        method: 'POST',
        headers: {
          'Authorization': "Bearer " + accessToken,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .catch((err) => err);
}

export function deleteApi(model, accessToken) {
    return fetch(`https://www.googleapis.com/calendar/v3/${model}`, {
        method: 'DELETE',
        headers: {
          'Authorization': "Bearer " + accessToken,
        },
      })
        .catch((err) => err);
}