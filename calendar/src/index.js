import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Redirect } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { configure } from 'mobx';
import * as serviceWorker from './serviceWorker';

import { LoginContainer } from './containers/LoginContainer.js';
import { CalendarContainer } from './containers/CalendarContainer.js';
import PrivateRoute from './components/PrivateRouteComponent';
import { css } from 'emotion';

const body = css`
    font-family: Sans-serif;
    width: 80%;
    margin-left: 10%;
    -webkit-box-shadow: 0px 0px 17px -5px #000000; 
    box-shadow: 0px 0px 17px -5px #000000;
    background-color: white;
`;


configure({ enforceActions: "observed" });

ReactDOM.render(
(
    <div className={body}>
    <BrowserRouter>
        <Route exact path="/" render={() => (
            !sessionStorage.getItem("email") ? (
            <Redirect to="/login"/>
            ) : (
            <Redirect to="/calendar"/>
            )
        )}/>
        <Route exact path="/login" component={LoginContainer} />
        <PrivateRoute exact path="/calendar" component={CalendarContainer} />
    </BrowserRouter>
    </div>
),document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
